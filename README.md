## Prerequisites

1. PHP 7.1 or later
2. Composer for dependancy management

### Start and run the application

```
$ composer install
$ npm run install
$ npm run production / npm run dev
$ docker-compose up -d
$ docker-compose exec app bash
$ php artisan migrate
$ exit

```