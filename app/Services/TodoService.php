<?php


namespace App\Services;


use App\Todo;
use Illuminate\Database\Eloquent\Collection;

class TodoService
{
    /**
     * @var Todo $repository
     */
    private $repository;

    /**
     * TodoService constructor.
     * @param Todo $repository
     */
    public function __construct(Todo $repository)
    {
        $this->repository = $repository;
    }

    public function all(): Collection
    {
        return $this->repository->all();
    }
}
