<?php


namespace App\Http\Controllers;


use App\Http\Requests\CreateTodoRequest;
use App\Http\Requests\UpdateTodoRequest;
use App\Services\TodoService;
use App\Todo;
use Illuminate\Support\Facades\Request;


class TodoApiController extends Controller
{
    /**
     * @var Todo $todo
     */
    private $todo;

    /**
     * TodoApiController constructor.
     * @param Todo $todo
     */
    public function __construct(Todo $todo)
    {
        $this->todo = $todo;
    }


    public function index()
    {
        return response()->json($this->todo->all());
    }

    public function create(CreateTodoRequest $request)
    {
        $todo = new Todo();
        $todo->name = $request->get('name');
        $todo->completed = 0;
        $todo->save();
        return response()->json($todo);
    }

    public function update(UpdateTodoRequest $request, $id)
    {

        /**
         * Todo $todo
         */
        $todo = $this->todo->find($id);
        if($todo == null){
            return response()->json(["message" => "task not found"]);
        }
        $todo->completed = $request->get('status');
        $todo->save();
        return $todo;
    }

    public function delete($id)
    {
        $todo = $this->todo->find($id);
        if($todo == null){
            return response()->json(["message" => "task not found"]);
        }
        $todo->delete();
        return response()->noContent();
    }
}
